#include "GoL.h"
#include <numeric>
#include <time.h>
#include <iostream>
#include <future>

constexpr int perc = 30;

template<Boundary bound>
void testForBoundary()
{
	std::string bname;
	switch (bound)
	{
	case Dead:
		bname = "Dead";
		break;
	case Alive:
		bname = "Alive";
		break;
	case Periodic:
		bname = "Periodic";
		break;
	case Random:
		bname = "Random";
		break;
	}
	std::ofstream out(bname);
	testForPerc<bound, 5>(out);
	testForPerc<bound, 10>(out);
	testForPerc<bound, 15>(out);
	testForPerc<bound, 20>(out);
	testForPerc<bound, 25>(out);
	testForPerc<bound, 30>(out);
	testForPerc<bound, 35>(out);
	testForPerc<bound, 40>(out);
	testForPerc<bound, 45>(out);
	testForPerc<bound, 50>(out);
	testForPerc<bound, 55>(out);
	testForPerc<bound, 60>(out);
	testForPerc<bound, 65>(out);
	testForPerc<bound, 70>(out);
	testForPerc<bound, 75>(out);
	testForPerc<bound, 80>(out);
	testForPerc<bound, 85>(out);
	testForPerc<bound, 90>(out);
	testForPerc<bound, 95>(out);

	out.close();
}

template<Boundary bound, int P>
void testForPerc(std::ostream& out)
{
	std::vector<std::future<std::vector<double> > > futures(5);

	for (int N = 0; N <= 4; N++)
		futures[N] = std::async(testForPercOfN<bound, P>, N);

	for (int N = 0; N <= 4; N++)
	{
		auto ret = futures[N].get();
		out << N;
		std::for_each(ret.begin(), ret.end(), [&out](auto& x) {out << " \t" << x; });
		out << "\n";
	}
}

template<Boundary bound, int P>
std::vector<double> testForPercOfN(int N)
{
	int all;
	bool changed;

	auto lambda = [&N, &changed, &all](std::vector<Field<P>> f)
	{
		//The number of neighbours alive
		const int neighbours = std::accumulate(f.begin(), f.end(), (int)-f[4]);
		//Whether the cell lives in the next state
		const bool lives = neighbours > N || neighbours == N && f[4].val;
		//If there was a change then the simulation is not over
		changed |= lives ^ f[4];
		//Increase the number of living if it lives
		all += lives;
		//Return
		return Field<P>(lives);
	};

	double avgTime = 0, avgArea = 0, avgSurv = 0;

	const int maxRun = 1000;

	for (int run = 0; run < maxRun; run++)
	{
		GoL<50, 50, bound, decltype(lambda), Field<P>> game(lambda, Randomize);

		changed = true;
		int i = 0;

		//It might be periodic
		const int maxIter = 500;

		while (changed && i < maxIter)
		{
			//Reset change and # of living
			changed = false;
			all = 0;
			//Write previous step
			//game.writeToPNM("Images/periodic" + std::to_string(N) + "_" + std::to_string(i) + ".pbm");
			//Step the simulation
			game.step();
			//Update index
			i++;
		}

		//We failed to find the end, try again
		if (i == maxIter)
		{
			i--;
			continue;
		}

		avgTime += i;
		avgArea += all;
		avgSurv += all > 0;
	}

	avgTime /= maxRun;
	avgArea /= maxRun;
	avgSurv /= maxRun;

	return{ P, avgTime, avgArea, avgSurv };
}

void gameOfLife()
{
	auto gameOfLife = [](std::vector<Field<perc>> f)
	{
		int neighbours = std::accumulate(f.begin(), f.end(), (int)-f[4]);

		return Field<perc>(neighbours >= 2 && neighbours < 4 && (f[4].val || neighbours == 3 && !f[4].val));
	};

	GoL<50, 50, Boundary::Random, decltype(gameOfLife), Field<perc>> game(gameOfLife, Empty);

	for (int i = 0; i < 5000; i++)
	{
		game.writeToPNM("Images/game1_" + std::to_string(i) + ".pbm");

		game.step();
	}

	GoL<50, 50, Boundary::RandomChanging, decltype(gameOfLife), Field<perc>> gol_rand(gameOfLife, Empty);

	for (int i = 0; i < 5000; i++)
	{
		gol_rand.writeToPNM("Images/gol_rand_" + std::to_string(i) + ".pbm");

		gol_rand.step();
	}

	GoL<7, 7, Boundary::Periodic, decltype(gameOfLife), Field<perc>> game2(gameOfLife, "glider.dat");

	for (int i = 0; i < 100; i++)
	{
		game2.writeToPNM("Images/game2_" + std::to_string(i) + ".pbm");

		game2.step();
	}

	GoL<45, 30, Boundary::Dead, decltype(gameOfLife), Field<perc>> gun(gameOfLife, "glider gun.txt");

	for (int i = 0; i < 100; i++)
	{
		gun.writeToPNM("Images/gun_" + std::to_string(i) + ".pbm");

		gun.step();
	}

	GoL<100, 100, Boundary::Periodic, decltype(gameOfLife), Field<perc>> game3(gameOfLife, Randomize);

	for (int i = 0; i < 2000; i++)
	{
		game3.writeToPNM("Images/game3_" + std::to_string(i) + ".pbm");

		game3.step();
	}
}

void sandPile()
{

}

int main()
{
	srand(time(0));

	//gameOfLife();
	
	/*
	testForBoundary<Dead>();
	testForBoundary<Alive>();
	testForBoundary<Periodic>();
	testForBoundary<Random>();
	*/
}