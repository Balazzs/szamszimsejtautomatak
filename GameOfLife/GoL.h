#pragma once
#include <array>
#include <vector>
#include <algorithm>
#include <random>
#include <fstream>
#include <string>

enum Boundary {
	Dead,
	Alive,
	Random,
	RandomChanging,
	Periodic
};

enum StartCondition {
	Empty,
	Full,
	Randomize
};

//The percentage of living for rand
template<int perc>
struct Field {
	bool val;

	Field() {}
	Field(bool val) : val(val) {}

	static Field dead() { return Field(false); }
	static Field alive() { return Field(true); }
	static Field random() {  return Field( (bool) (rand() % 100 < perc ) ); }

	const static std::string PNMMagicNumber;
	int getPNMVal() const { return val ? 0 : 1 ; }

	operator int()
	{
		return val ? 1 : 0;
	}
};

template<int perc>
const std::string Field<perc>::PNMMagicNumber = "P1";

template<int perc>
std::ifstream& operator>>(std::ifstream& str, Field<perc>& f)
{
	int t;
	str >> t;
	f = Field<perc>( !!t);
	return str;
}

template<int perc>
std::ofstream& operator<<(std::ofstream& str, const Field<perc>& f)
{
	str << f.getPNMVal();
	return str;
}

template<size_t N, size_t M, Boundary bound, typename RuleType, typename CellType = Field<50>, class RowCont = std::array<CellType, N+2>, class Cont = std::array<RowCont, M+2>>
class GoL
{
	RuleType rule;

	Cont data;

	//Get boundaryCell
	//Returns a cell that copies [yFrom][xFrom] if the type is periodic
	CellType getBoundaryCell(size_t xFrom, size_t yFrom)
	{
		switch (bound)
		{
		case Dead:
			return CellType::dead();
		case Alive:
			return CellType::alive();
		case Random:
		case RandomChanging:
			return CellType::random();
		case Periodic:
			return data[yFrom][xFrom];
		}
	}

	//Fill the boundary data initially
	//In case of Dead/alive/random the fill only needs to be done once
	void fillBoundary()
	{
		for (size_t x = 1; x < N + 1; x++)
		{
			data[0][x] = getBoundaryCell(x, M);
			data[M + 1][x] = getBoundaryCell(x, 1);
		}
		for (size_t y = 1; y < M + 1; y++)
		{
			data[y][0] = getBoundaryCell(N, y);
			data[y][N + 1] = getBoundaryCell(1, y);
		}

		//The 4 corners are special cases for the periodic boundary
		data[0][0] = getBoundaryCell(N, M);
		data[M + 1][0] = getBoundaryCell(N, 1);
		data[0][N + 1] = getBoundaryCell(1, M);
		data[M + 1][N + 1] = getBoundaryCell(1, 1);
	}

	//If the boundary has to be updated
	void updateBoundary()
	{
		switch (bound)
		{
		case Dead:
		case Alive:
		case Random:
			return;
		case RandomChanging:
			fillBoundary();
		case Periodic:
			fillBoundary();
		}
	}

	static CellType getCondition(StartCondition cond)
	{
		switch (cond)
		{
		case Empty:
			return CellType::dead();
		case Full:
			return CellType::alive();
		case Randomize:
			return CellType::random();
		}
	}

public:
	//Default is fill with random
	GoL(const RuleType& rule, StartCondition cond = Randomize) : rule(rule)
	{
		for (size_t y = 0; y < M; y++)
		{
			for (size_t x = 0; x < N; x++)
				data[y + 1][x + 1] = getCondition(cond);
		}

		//Initial fill of boundary
		fillBoundary();
	}

	GoL(RuleType rule, const std::vector< std::vector< CellType > >& defaultState) : rule(rule)
	{
		if (defaultState.size() != M)
			std::runtime_error("Wrong init data size!");

		for (size_t y = 0; y < M; y++)
		{
			if (defaultState[y].size() != N)
				std::runtime_error("Wrong init data size!");

			for (size_t x = 0; x < N; x++)
				data[y + 1][x + 1] = defaultState[y][x];
		}

		//Initial fill of boundary
		fillBoundary();
	}

	//Load from file
	GoL(RuleType rule, const std::string& fname) : rule(rule)
	{
		std::ifstream str(fname);

		for (size_t y = 0; y < M + 2; y++)
			for (size_t x = 0; x < N + 2; x++)
				str >> data[y][x];

		str.close();
	}

	void step()
	{
		//Create the new data
		//We can't use the previous state for updating since updating the cells would invalidate the neighbours of its neighbours
		//It would be highly update order dependent
		Cont nextStep = data;

		for (int y = 1; y <= M; y++)
			for (int x = 1; x <= N; x++)
				nextStep[y][x] = rule({ data[y-1][x-1], data[y-1][x], data[y-1][x+1], data[y][x-1], data[y][x], data[y][x+1], data[y+1][x-1], data[y+1][x], data[y+1][x+1]  });

		//Copy back the new values
		data = std::move(nextStep);

		//Update the boundary
		updateBoundary();
	}

	void writeToPNM(const std::string& fname) const
	{
		auto str = std::ofstream(fname);

		str << CellType::PNMMagicNumber << "\n";
		
		int periodic = (bound == Periodic) ? 1 : 0;

		str << N + 2 * !periodic << " " << M + 2 * !periodic << "\n";

		for (size_t y = 0 + periodic; y < M + 1 + !periodic; y++)
		{
			for (size_t x = 0 + periodic; x < N + 1 + !periodic; x++)
				str << data[y][x] << " ";

			str << "\n";
		}

		str.close();
	}

};

